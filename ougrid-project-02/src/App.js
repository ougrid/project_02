import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
   




///Note
// {
//   "type": "template",
//   "altText": "This is a buttons template",
//   "template": {
//     "type": "buttons",
//     "thumbnailImageUrl": "https://example.com/bot/images/image.jpg",
//     "imageAspectRatio": "rectangle",
//     "imageSize": "cover",
//     "imageBackgroundColor": "#FFFFFF",
//     "title": "Menu",
//     "text": "Please select",
//     "defaultAction": {
//       "type": "uri",
//       "label": "View detail",
//       "uri": "http://example.com/page/123"
//     },
//     "actions": [
//       {
//         "type": "postback",
//         "label": "Buy",
//         "data": "action=buy&itemid=123"
//       },
//       {
//         "type": "postback",
//         "label": "Add to cart",
//         "data": "action=add&itemid=123"
//       },
//       {
//         "type": "uri",
//         "label": "View detail",
//         "uri": "http://example.com/page/123"
//       }
//     ]
//   }
// }



// {
//   "type": "text",
//   "text": "Hello, world1",
//   "sender": {
//       "name": "Bot",
//       "iconUrl": "https://stickershop.line-scdn.net/stickershop/v1/sticker/51626526/ANDROID/sticker.png"
//   }
// },
// {
//   "type": "text",
//   "text": "Hello, world2"
// },
// {
//   "type": "text",
//   "text": "$ LINE emoji $",
//   "emojis": [
//       {
//           "index": 0,
//           "productId": "5ac1bfd5040ab15980c9b435",
//           "emojiId": "001"
//       },
//       {
//           "index": 13,
//           "productId": "5ac1bfd5040ab15980c9b435",
//           "emojiId": "002"
//       }
//   ]
// },
// {
//   "type": "text",
//   "text": "\uDBC0\uDC84 LINE original emoji"
// },
// {
//   "type": "location",
//   "title": "my location",
//   "address": "1-6-1 Yotsuya, Shinjuku-ku, Tokyo, 160-0004, Japan",
//   "latitude": 35.687574,
//   "longitude": 139.72922
// }